FROM centos  
MAINTAINER Oliver Carr <oli@ozoli.io>

ENV LANG C.UTF-8
ENV JAVA_PACKAGE_URL https://download.java.net/java/early_access/jdk11/14/GPL/openjdk-11-ea+14_linux-x64_bin.tar.gz 

# Upgrading system
RUN yum -y upgrade
RUN yum -y install curl
RUN yum -y install tar
RUN yum -y install wget

WORKDIR /opt

RUN wget --no-cookies --no-check-certificate \
  $JAVA_PACKAGE_URL -O /opt/jdk11.tar.gz \
  && tar -zxvf jdk11.tar.gz \
  && rm jdk11.tar.gz

ENV JAVA_HOME /opt/jdk-11
ENV PATH="${JAVA_HOME}/bin:${PATH}"

RUN useradd -ms /bin/bash javauser

ARG MAVEN_VERSION=3.5.3
ARG USER_HOME_DIR="/home/javauser"
ARG SHA=b52956373fab1dd4277926507ab189fb797b3bc51a2a267a193c931fffad8408
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

# Maven fails with 'Can't read cryptographic policy directory: unlimited'
# because it looks for $JAVA_HOME/conf/security/policy/unlimited but it is in
# /etc/java-9-openjdk/security/policy/unlimited
#RUN ln -s /etc/java-10-openjdk /usr/lib/jvm/java-10-openjdk-$(dpkg --print-architecture)/conf

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

COPY mvn-entrypoint.sh /usr/local/bin/mvn-entrypoint.sh
COPY settings-docker.xml /usr/share/maven/ref/

#RUN useradd -ms /bin/bash javauser
WORKDIR /home/javauser
USER javauser

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn", "--version"]

