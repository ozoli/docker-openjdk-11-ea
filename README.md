# OpenJDK Early Access Docker Image with Maven

## Prior Work
This Dockerfile is based on the following projects:
- [Update to Java 11 now!](http://dev.solita.fi/2018/03/02/Update-to-Java11.html)
- [Docker Maven](https://github.com/carlossg/docker-maven)

## Contents

- [Open JDK 11 Early Access](http://jdk.java.net/11/) build openjdk-11-ea-14
- [Maven 3.5.3](http://maven.apache.org/ref/3.5.3/)

## Issues

Feedback or issues are most welcome, please use the issues link [here](https://gitlab.com/ozoli/docker-openjdk-11-ea/issues).
